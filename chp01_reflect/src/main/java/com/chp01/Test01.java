package com.chp01;

import java.lang.reflect.Field;

public class Test01 {

    public static void main(String[] args) {
        //获取Student的类对象
        Class clz = Student.class;
        System.out.println(clz);
        Field[] fields = clz.getDeclaredFields();
        for(int i = 0; i < fields.length; i++){
            Field field = fields[i];
            System.out.println(field.getName()+"\t"+field.getType()+"\t"+field.getModifiers());
        }
    }
}
