package com.chp01;

import java.lang.reflect.Field;

public class Test03 {

    public static void main(String[] args) throws ClassNotFoundException {
        //通过类去获取
        Class clz01 = Student.class;

        Student student = new Student();
        //通过对象获取
        Class clz02 = student.getClass();

        String className = "com.chp01.Student";
        //重要---通过字符串获取--灵活
        Class clz03 = Class.forName(className);
        for(Field field:clz03.getDeclaredFields()) {
            System.out.println(field.getName());
        }
    }
}
