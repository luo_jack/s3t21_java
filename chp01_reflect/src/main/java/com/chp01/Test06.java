package com.chp01;

import java.lang.reflect.Method;

public class Test06 {
    public static void main(String[] args) throws Exception {
        //通过setXXX进行赋值
        Class clz = Class.forName("com.chp01.Student");
        Object obj = clz.newInstance();
        Object obj2 = clz.newInstance();

        Method method01 = clz.getDeclaredMethod("setName",String.class);
        method01.invoke(obj,"Mike");
        method01.invoke(obj2,"Rose");
        Method method02 = clz.getDeclaredMethod("setAge",int.class);
        method02.invoke(obj,99);
        method02.invoke(obj2,17);
        Method method03 = clz.getDeclaredMethod("setGender",String.class);
        method03.invoke(obj,"boy");
        method03.invoke(obj2,"girl");
        Method methodShow = clz.getDeclaredMethod("show");
        methodShow.invoke(obj);
        methodShow.invoke(obj2);
    }
}
