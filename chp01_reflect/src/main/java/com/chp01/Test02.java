package com.chp01;

import java.lang.reflect.Method;

public class Test02 {
    public static void main(String[] args) {
        //打印全部方法
        Class clz = Student.class;
        Method[] methods = clz.getDeclaredMethods();
        for(Method method:methods) {
            System.out.println(method.getName());
        }
    }
}
