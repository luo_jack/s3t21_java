package com.chp01;

import java.lang.reflect.Method;

public class Test04 {

    public static void main(String[] args) throws Exception {
        //通过反射创建对象
        Class clz = Class.forName("com.chp01.Student");
        //创建实例
        Object obj = clz.newInstance();

        //获取需要被调用方法
        Method method = clz.getDeclaredMethod("hello");
        //设置方法的可访问性
//        method.setAccessible(true);
        //调用方法
        method.invoke(obj);
    }
}
