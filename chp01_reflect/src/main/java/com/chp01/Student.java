package com.chp01;

public class Student {

    public String name;

    public int age;

    private String gender;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void show(){
        System.out.println("姓名:"+name+" 年龄:"+age+" 性别:"+gender);
    }

    public void hello(){
        System.out.println("大家好");
    }
}
