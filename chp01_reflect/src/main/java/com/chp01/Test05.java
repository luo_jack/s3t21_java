package com.chp01;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test05 {

    public static void main(String[] args) throws Exception {
        Student student = new Student();
        student.name = "Jack";
        student.age = 19;
        student.show();

        Class clz = Class.forName("com.chp01.Student");
        Object obj = clz.newInstance();

        Field fieldName = clz.getDeclaredField("name");
        fieldName.set(obj,"Tom");
        Field fieldAge = clz.getDeclaredField("age");
        fieldAge.set(obj,22);
        Field fieldGender = clz.getDeclaredField("gender");
        fieldGender.setAccessible(true);
        fieldGender.set(obj,"boy");
        Method methodShow = clz.getDeclaredMethod("show");
        methodShow.invoke(obj);
    }
}
