package dom4j.book;

import java.util.ArrayList;
import java.util.List;

public class Book {

    private BookTitle title;

    private String category;

    private String cover;

    private List<String> authors = new ArrayList<>();

    private int year;

    private double price;

    public void addAuthor(String author) {
        this.authors.add(author);
    }

    public void addTitle(String title,String lang){
        BookTitle bookTitle = new BookTitle();
        bookTitle.setContent(title);
        bookTitle.setLang(lang);
        this.setTitle(bookTitle);
    }

    public BookTitle getTitle() {
        return title;
    }

    public void setTitle(BookTitle title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title=" + title +
                ", category='" + category + '\'' +
                ", cover='" + cover + '\'' +
                ", authors=" + authors +
                ", year=" + year +
                ", price=" + price +
                '}';
    }


}
