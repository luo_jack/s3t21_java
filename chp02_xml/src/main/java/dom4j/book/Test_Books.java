package dom4j.book;

import dom4j.TestA_DOM4J;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class Test_Books {
    public static void main(String[] args) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(TestA_DOM4J.class.getClassLoader().getResourceAsStream("books.xml"));
        Element root = document.getRootElement();
        List<Element> bookElements = root.elements("book");
        for (Element bookElem : bookElements){
            String category = bookElem.attributeValue("category");
            String cover = bookElem.attributeValue("cover");
            String title = bookElem.elementText("title");
            String lang = bookElem.element("title").attributeValue("lang");
            System.out.println(title+"\t"+lang);
            List<Element> authorElements = bookElem.elements("author");
            for (Element authorElem : authorElements){
                System.out.print(authorElem.getText()+",");
            }
            System.out.println();
            String year = bookElem.elementText("year");
            String price = bookElem.elementText("price");
            System.out.println("year:"+year+"\tprice:"+price);
            System.out.println();
        }
    }
}
