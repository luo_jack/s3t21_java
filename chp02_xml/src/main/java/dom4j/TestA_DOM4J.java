package dom4j;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class TestA_DOM4J {

    public static void main(String[] args) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(TestA_DOM4J.class.getClassLoader().getResourceAsStream("a.xml"));
        Element root = document.getRootElement();
        List<Element> elementList = root.elements("stu");
        for(Element element : elementList){
            String name = element.attributeValue("name");
            String age = element.attributeValue("age");
            String gender = element.attributeValue("gender");
            System.out.println(name+"\t"+age+"\t"+gender);
        }
    }
}
