package dom4j;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileWriter;

public class TestB_Write {

    public static void main(String[] args) throws Exception {
        String name = "彼得";
        String age = "19";
        String gender = "boy";
        SAXReader reader = new SAXReader();
        Document document = reader.read(TestA_DOM4J.class.getClassLoader().getResourceAsStream("b.xml"));
        Element root = document.getRootElement();
        Element elementStu = root.addElement("stu");

        elementStu.addElement("name").addText(name);
        elementStu.addElement("age").addText(age);
        elementStu.addElement("gender").addText(gender);

        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        XMLWriter writer = new XMLWriter(
                new FileWriter(TestA_DOM4J.class.getClassLoader().getResource("b.xml").getPath()),
                format);
        writer.write(document);
        writer.close();
    }
}
