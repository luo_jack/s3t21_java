package dom4j;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class TestB_DOM4J {

    public static void main(String[] args) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(TestA_DOM4J.class.getClassLoader().getResourceAsStream("b.xml"));
        Element root = document.getRootElement();
        List<Element> elementList = root.elements("stu");
        for(Element element:elementList){
            String name = element.elementText("name");
            String age = element.elementText("age");
            String gender = element.elementText("gender");
            System.out.println(name+"\t"+age+"\t"+gender);
        }
    }
}
