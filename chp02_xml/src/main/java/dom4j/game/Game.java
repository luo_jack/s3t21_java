package dom4j.game;

import java.sql.Date;

public class Game {

    private String title;

    private String link;

    private Date pubDate;

    private String author;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Game{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", pubDate=" + pubDate +
                ", author='" + author + '\'' +
                '}';
    }
}
