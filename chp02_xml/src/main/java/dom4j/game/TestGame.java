package dom4j.game;

import java.util.List;

public class TestGame {

    public static void main(String[] args) throws Exception {
        GameDao gameDao = new GameDao();
        List<Game> games = gameDao.getAll();
        for(Game game:games){
            System.out.println(game);
        }
    }
}
