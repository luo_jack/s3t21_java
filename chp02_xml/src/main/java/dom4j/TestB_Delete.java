package dom4j;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileWriter;

public class TestB_Delete {

    public static void main(String[] args) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(TestA_DOM4J.class.getClassLoader().getResourceAsStream("b.xml"));
        Element root = document.getRootElement();

//        Element element = root.elements("stu").get(1);
        //根据ID查找,ID必须大写
        Element element = document.elementByID("1001");
        root.remove(element);
//        element.getParent().remove(element);

        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        XMLWriter writer = new XMLWriter(
                new FileWriter(TestA_DOM4J.class.getClassLoader().getResource("b.xml").getPath()),
                format);
        writer.write(document);
        writer.close();
    }
}
