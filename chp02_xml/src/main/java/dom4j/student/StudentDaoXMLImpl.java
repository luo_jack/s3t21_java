package dom4j.student;

import dom4j.TestA_DOM4J;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoXMLImpl implements StudentDao{

    private Student trans(Element element){
        String name = element.elementText("name");
        String age = element.elementText("age");
        String gender = element.elementText("gender");
        String id = element.attributeValue("ID");
        Student stu = new Student();
        stu.setID(Integer.parseInt(id));
        stu.setAge(Integer.parseInt(age));
        stu.setName(name);
        stu.setGender(gender);
        return stu;
    }

    @Override
    public List<Student> getAll() {
        List<Student> students = new ArrayList<>();
        Document document = XMLUtils.getDocument("b.xml");
        Element root = document.getRootElement();
        List<Element> elementList = root.elements("stu");
        for(Element element:elementList){
            Student stu = this.trans(element);
            students.add(stu);
        }
        return students;
    }

    @Override
    public Student getByID(int id) {
        Document document = XMLUtils.getDocument("b.xml");
        Element element = document.elementByID(String.valueOf(id));
        if (element == null){
            return null;
        }
        Student student = this.trans(element);
        return student;
    }

    @Override
    public void add(Student student) {
        Document document = XMLUtils.getDocument("b.xml");
        Element root = document.getRootElement();
        Element element = document.elementByID(String.valueOf(student.getID()));
        if (element != null){
            throw new RuntimeException("学号已经存在");
        }

        Element elementStu = root.addElement("stu");

        elementStu.addElement("name").addText(student.getName());
        elementStu.addElement("age").addText(String.valueOf(student.getAge()));
        elementStu.addElement("gender").addText(student.getGender());
        elementStu.addAttribute("ID",String.valueOf(student.getID()));

        XMLUtils.save(document,"b.xml");
    }

    @Override
    public void delete(int id) {
        Document document = XMLUtils.getDocument("b.xml");
        Element element = document.elementByID(String.valueOf(id));
        if (element == null){
            throw new RuntimeException("学号不存在");
        }
        element.getParent().remove(element);
        XMLUtils.save(document,"b.xml");
    }

    @Override
    public void update(Student student) {
        Document document = XMLUtils.getDocument("b.xml");
        Element element = document.elementByID(String.valueOf(student.getID()));
        if (element == null){
            throw new RuntimeException("学号不存在");
        }
        element.element("gender").setText(student.getGender());
        element.element("name").setText(student.getName());
        element.element("age").setText(String.valueOf(student.getAge()));

        XMLUtils.save(document,"b.xml");
    }
}
