package dom4j.student;

import java.util.List;

public interface StudentDao {

    List<Student> getAll();

    Student getByID(int id);

    void add(Student student);

    void delete(int id);

    void update(Student student);
}
