package dom4j.student;

public class TestStudent {

    public static void main(String[] args) {
        StudentDao studentDao = new StudentDaoXMLImpl();
//        System.out.println(studentDao.getAll());

//        System.out.println(studentDao.getByID(1002));

//        Student student = new Student();
//        student.setID(1234);
//        student.setName("同伴们");
//        student.setAge(12);
//        student.setGender("boy");
//        studentDao.add(student);

//        studentDao.delete(1234);

        Student student = studentDao.getByID(1002);
        student.setAge(student.getAge()+1);
        studentDao.update(student);
    }
}
