package dom4j;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.FileWriter;

public class TestB_Update {

    public static void main(String[] args) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(TestA_DOM4J.class.getClassLoader().getResourceAsStream("b.xml"));
        Element root = document.getRootElement();

        //修改1002的性别为 boy
        Element element = document.elementByID("1002");
        element.element("gender").setText("boy");


        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        XMLWriter writer = new XMLWriter(
                new FileWriter(TestA_DOM4J.class.getClassLoader().getResource("b.xml").getPath()),
                format);
        writer.write(document);
        writer.close();
    }
}
