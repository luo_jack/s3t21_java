package dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class TestA_02 {

    public static void main(String[] args) throws Exception {
        //从根路径开始找
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(TestA_01.class.getClassLoader().getResourceAsStream("a.xml"));

        //获取根节点
        Element root = doc.getDocumentElement();
        //获取子节点
        NodeList nodeList = root.getChildNodes();
//        System.out.println(nodeList.getLength());
        for(int i = 0; i < nodeList.getLength(); i++){
            Node node = nodeList.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                System.out.println(element.getAttribute("name"));
            }
        }
    }
}
