package com.myschool.dao;

import com.myschool.entity.Subject;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SubjectDao {

    @Select("select * from subject")
    List<Subject> getAll();

    @Select("select * from subject where subjectNo=#{subjectNo}")
    Subject getOne(int subjectNo);

    @Insert("INSERT INTO `subject`(subjectName,classHour,gradeID) \n" +
            "VALUES(#{subjectName},#{classHour},#{gradeID})")
    void add(Subject subject);

    @Update("UPDATE \n" +
            "  `subject` \n" +
            "SET\n" +
            "  `subjectName` = #{subjectName},\n" +
            "  `classHour` = #{classHour},\n" +
            "  `gradeID` = #{gradeID} \n" +
            "WHERE `subjectNo` = #{subjectNo} ;")
    void update(Subject subject);

    @Delete("DELETE \n" +
            "FROM\n" +
            "  `myschool`.`subject` \n" +
            "WHERE `subjectNo` = #{subjectNo} ;")
    void delete(int subjectNo);
}
