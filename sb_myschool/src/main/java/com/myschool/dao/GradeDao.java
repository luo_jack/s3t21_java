package com.myschool.dao;

import com.myschool.entity.Grade;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface GradeDao {

    @Select("select * from grade")
    List<Grade> getAll();
}
