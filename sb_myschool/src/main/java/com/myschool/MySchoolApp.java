package com.myschool;

import com.myschool.dao.GradeDao;
import com.myschool.entity.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SpringBootApplication
@RestController
public class MySchoolApp {

    @Autowired
    GradeDao gradeDao;

    public static void main(String[] args) {
        SpringApplication.run(MySchoolApp.class,args);
    }

    @GetMapping("/hello")
    public String hello(){
        return "hello,world";
    }

    @GetMapping("/hello2")
    public Object hello2(){
        HashMap<String,Object> map = new HashMap<>();
        List<String> names = new ArrayList<>();
        names.add("jack");
        names.add("tom");
        names.add("mike");
        map.put("success",true);
        map.put("message","添加成功");
        map.put("data",names);
        return map;
    }

    @GetMapping("/hello3")
    public Object hello3(){

        return gradeDao.getAll();
    }
}
