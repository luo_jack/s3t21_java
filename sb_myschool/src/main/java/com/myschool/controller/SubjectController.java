package com.myschool.controller;

import com.myschool.dao.SubjectDao;
import com.myschool.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/subject")
public class SubjectController {

    @Autowired
    SubjectDao subjectDao;

    @GetMapping("/all")
    public Object getAll(){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("data",subjectDao.getAll());
        return map;
    }

    @GetMapping("/one")
    public Object getOne(int subjectNo){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("data",subjectDao.getOne(subjectNo));
        return map;
    }

    @PostMapping("/add")
    public Object add(@RequestBody Subject subject){
        subjectDao.add(subject);
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        return map;
    }

    @PostMapping("/update")
    public Object update(@RequestBody Subject subject){
        subjectDao.update(subject);
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        return map;
    }

    @PostMapping("/delete")
    public Object delete(int subjectNo){
        subjectDao.delete(subjectNo);
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        return map;
    }
}
