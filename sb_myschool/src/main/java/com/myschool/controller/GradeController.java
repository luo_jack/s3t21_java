package com.myschool.controller;

import com.myschool.dao.GradeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/grade")
public class GradeController {

    @Autowired
    GradeDao gradeDao;

    @GetMapping("/all")
    public Object all(){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("message",null);
        map.put("data",gradeDao.getAll());
        return map;
    }
}
