<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1>${empty names}</h1>
<c:forEach items="${names}" var="name">
    <h2>${name}</h2>
</c:forEach>
</body>
</html>