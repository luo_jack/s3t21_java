<%--
  Created by IntelliJ IDEA.
  User: 17400
  Date: 2023/5/15
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>文件上传</title>
    <script>
        function getFile(event) {
            if (!event) return;
            let fileList= event.target.files;
            if (fileList.length <= 0) return;
            const fileReader = new FileReader();
            fileReader.readAsDataURL(fileList[0]);
            fileReader.onload = (event) => {
                document.getElementById("preview").src =event.target.result;
            };
        }
    </script>
</head>
<body>
    <h1>${url}</h1>
    <img src="http://${url}"/>
    <form
            method="post"
            action="${pageContext.request.contextPath}/upload/add"
            enctype="multipart/form-data"
    >
        <input type="file" name="mypic"
               accept=".png,.jpg,.jpeg,.ico,.gif,.bpm"
               onchange="getFile(event)">
        <img id="preview" src="" alt="">
        <button type="submit">提交</button>
    </form>
</body>
</html>
