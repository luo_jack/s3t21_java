package com.example.converter;

import com.example.entity.Student;
import org.springframework.core.convert.converter.Converter;

public class String2StudentConverter implements Converter<String, Student> {
    @Override
    public Student convert(String s) {
        String[] arr = s.split("_");
        Student student = new Student();
        student.setId(Integer.valueOf(arr[0]));
        student.setName(arr[1]);
        student.setAge(Integer.valueOf(arr[2]));
        return student;
    }
}
