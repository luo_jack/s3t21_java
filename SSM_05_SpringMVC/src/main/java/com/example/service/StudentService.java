package com.example.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    public StudentService(){
        System.out.println("StudentService...构造函数");
    }

    public List<String> getAll(){
        List<String> names = new ArrayList<>();
        names.add("杰克");
        names.add("皮特");
        names.add("汤姆");
        return names;
    }

    public List<String> getAll2(){
        List<String> names = new ArrayList<>();
        names.add("jack");
        names.add("peter");
        names.add("tom");
        return names;
    }
}
