package com.example.controller;

import com.example.entity.Student;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/stu")
public class StudentController {

    @PostMapping
    public Object add(){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("action","add");
        return map;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable Integer id){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("action","delete");
        map.put("id",id);
        return map;
    }

    @PutMapping
    public Object update(@Valid Student student){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("action","update");
        map.put("data",student);
        return map;
    }

    @GetMapping
    public Object query() throws ParseException {
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("action","query");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Student> studentList = new ArrayList<>();
        Student stu1 = new Student();
        stu1.setId(1001);
        stu1.setName("杰克");
        stu1.setAge(18);
        stu1.setBornDate(simpleDateFormat.parse("2001-1-2 10:00:00"));
        Student stu2 = new Student();
        stu2.setId(1005);
        stu2.setName("佩特");
        stu2.setAge(23);
        stu2.setBornDate(simpleDateFormat.parse("2002-1-2 9:00:00"));
        studentList.add(stu1);
        studentList.add(stu2);
        map.put("data",studentList);
        return map;
    }
}
