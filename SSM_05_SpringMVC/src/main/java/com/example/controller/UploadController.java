package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

@Controller
@RequestMapping("/upload")
public class UploadController {
    private static final Logger log = Logger.getLogger(UploadController.class);

    @RequestMapping("/pre")
    public String uploadPre(){
        return "upload";
    }

    String qiniuurl = "ruoibo3uv.hn-bkt.clouddn.com";

    String accessKey = "KbHHkisAfThgJFMTCU0zgNZv3MjG6WqwfDqC-7iU";

    String secretKey = "6rhILT-yRO1cOue5hmLZKtzgGBaQnKiy0XMAeSvN";

    String bucket = "new515";

    @PostMapping("/add")
    public ModelAndView doUpload(HttpServletRequest request, MultipartFile mypic){
        ModelAndView modelAndView = new ModelAndView("upload");
//构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region2());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
//...其他参数参考类注释

        UploadManager uploadManager = new UploadManager(cfg);
//...生成上传凭证，然后准备上传

//如果是Windows情况下，格式是 D:\\qiniu\\test.png
//        String localFilePath = "/home/qiniu/test.png";
//默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = mypic.getOriginalFilename();
        log.info(key);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);

        try {

            Response response = uploadManager.put(mypic.getBytes(), key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
//            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            log.info(putRet.key);
            log.info(putRet.hash);
            String url = qiniuurl + "/" + key;
            log.info("url:"+url);
            modelAndView.addObject("url",url);
        } catch (QiniuException ex) {
            Response r = ex.response;
            log.error(r.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return modelAndView;
    }

    @CrossOrigin(origins = "*",maxAge = 3600)
    @PostMapping("/add2")
    @ResponseBody
    public Object doUpload2(MultipartFile mypic){
        HashMap<String,Object> map = new HashMap<>();
//构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region2());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
//...其他参数参考类注释

        UploadManager uploadManager = new UploadManager(cfg);
//...生成上传凭证，然后准备上传

//如果是Windows情况下，格式是 D:\\qiniu\\test.png
//        String localFilePath = "/home/qiniu/test.png";
//默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = mypic.getOriginalFilename();
        log.info(key);
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);

        try {

            Response response = uploadManager.put(mypic.getBytes(), key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = JSON.parseObject(response.bodyString(), DefaultPutRet.class);
//            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            log.info(putRet.key);
            log.info(putRet.hash);
            String url = "http://" + qiniuurl + "/" + key;
            log.info("url:"+url);
            map.put("name",key);
            map.put("status","done");
            map.put("url",url);
            map.put("thumbUrl",url);
        } catch (QiniuException ex) {
            Response r = ex.response;
            log.error(r.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }
}
