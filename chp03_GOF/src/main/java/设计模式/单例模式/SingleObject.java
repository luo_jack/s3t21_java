package 设计模式.单例模式;

public class SingleObject {

    //让构造函数为 private，这样该类就不会被实例化
    private SingleObject(){}

    private static SingleObject singleObject;

    public static SingleObject getInstance(){
        if(singleObject == null){
            singleObject = new SingleObject();
        }
        return singleObject;
    }
}
