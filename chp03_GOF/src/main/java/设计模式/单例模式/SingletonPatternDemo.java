package 设计模式.单例模式;

public class SingletonPatternDemo {

    public static void main(String[] args) {
        SingleObject singleObject1 = SingleObject.getInstance();
        SingleObject singleObject2 = SingleObject.getInstance();
        System.out.println(singleObject1 == singleObject2);
    }
}
