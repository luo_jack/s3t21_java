package 设计模式.抽象工厂模式.student;

public class StudentDaoOracleImpl implements StudentDao{
    @Override
    public void getAll() {
        System.out.println("oracle -- student -- all");
    }
}
