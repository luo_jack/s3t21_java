package 设计模式.抽象工厂模式;

import 设计模式.抽象工厂模式.grade.GradeDao;
import 设计模式.抽象工厂模式.grade.GradeDaoMySqlImpl;
import 设计模式.抽象工厂模式.student.StudentDao;
import 设计模式.抽象工厂模式.student.StudentDaoMySqlImpl;

public class MySqlFactory implements AbstractFactory{
    @Override
    public GradeDao getGradeDaoInstance() {
        return new GradeDaoMySqlImpl();
    }

    @Override
    public StudentDao getStudentDaoInstance() {
        return new StudentDaoMySqlImpl();
    }
}
