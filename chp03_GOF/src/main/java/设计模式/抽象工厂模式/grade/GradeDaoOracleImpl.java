package 设计模式.抽象工厂模式.grade;

public class GradeDaoOracleImpl implements GradeDao{
    @Override
    public void getAll() {
        System.out.println("oracle -- grade -- all");
    }
}
