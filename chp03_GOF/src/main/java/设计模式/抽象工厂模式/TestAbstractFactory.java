package 设计模式.抽象工厂模式;

import 设计模式.抽象工厂模式.grade.GradeDao;
import 设计模式.抽象工厂模式.grade.GradeDaoMySqlImpl;
import 设计模式.抽象工厂模式.student.StudentDao;
import 设计模式.抽象工厂模式.student.StudentDaoMySqlImpl;
import 设计模式.抽象工厂模式.student.StudentDaoOracleImpl;

public class TestAbstractFactory {

    public static void main(String[] args) {
        AbstractFactory factory = new OracleFactory();

        GradeDao gradeDao = factory.getGradeDaoInstance();

        StudentDao studentDao = factory.getStudentDaoInstance();

        gradeDao.getAll();
        studentDao.getAll();
    }
}
