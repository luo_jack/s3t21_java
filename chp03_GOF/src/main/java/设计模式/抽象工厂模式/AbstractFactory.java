package 设计模式.抽象工厂模式;

import 设计模式.抽象工厂模式.grade.GradeDao;
import 设计模式.抽象工厂模式.student.StudentDao;

//抽象工厂
public interface AbstractFactory {

    GradeDao getGradeDaoInstance();

    StudentDao getStudentDaoInstance();
}
