package 设计模式.抽象工厂模式;

import 设计模式.抽象工厂模式.grade.GradeDao;
import 设计模式.抽象工厂模式.grade.GradeDaoOracleImpl;
import 设计模式.抽象工厂模式.student.StudentDao;
import 设计模式.抽象工厂模式.student.StudentDaoOracleImpl;

public class OracleFactory implements AbstractFactory{
    @Override
    public GradeDao getGradeDaoInstance() {
        return new GradeDaoOracleImpl();
    }

    @Override
    public StudentDao getStudentDaoInstance() {
        return new StudentDaoOracleImpl();
    }
}
