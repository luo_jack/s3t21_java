package 设计模式.代理.动态代理.jdk;

public interface IRentHouse {

    void rentHouse();

    void clean();
}
