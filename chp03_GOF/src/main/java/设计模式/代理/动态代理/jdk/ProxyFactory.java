package 设计模式.代理.动态代理.jdk;

import java.lang.reflect.Proxy;

public class ProxyFactory {

    public static Object create(IRentHouse target){
        RentHouseHandle handle = new RentHouseHandle(target);
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),handle);
    }
}
