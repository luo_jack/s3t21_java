package 设计模式.代理.动态代理.jdk;

public class RentHouse implements IRentHouse {

    public void abc(){
        System.out.println("实现租房");
    }

    @Override
    public void rentHouse() {
        abc();
    }

    @Override
    public void clean() {
        System.out.println("实现打扫");
    }
}
