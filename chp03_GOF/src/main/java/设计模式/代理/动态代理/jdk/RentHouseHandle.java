package 设计模式.代理.动态代理.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class RentHouseHandle implements InvocationHandler {

    private IRentHouse iRentHouse;

    public RentHouseHandle(IRentHouse iRentHouse){
        this.iRentHouse = iRentHouse;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("中介准备工作");
        System.out.println(method.getName());
        Object obj = method.invoke(iRentHouse,args);
        System.out.println("中介收尾工作");
        return obj;
    }
}
