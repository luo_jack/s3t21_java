package 设计模式.代理.动态代理.jdk;

public class TestJDK {

    public static void main(String[] args) {
        IRentHouse iRentHouse = new RentHouse();
        IRentHouse iRentHouseProxy = (IRentHouse) ProxyFactory.create(iRentHouse);
//        iRentHouseProxy.rentHouse();
        iRentHouseProxy.clean();
    }
}
