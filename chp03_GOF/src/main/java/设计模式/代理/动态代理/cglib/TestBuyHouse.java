package 设计模式.代理.动态代理.cglib;

public class TestBuyHouse {

    public static void main(String[] args) {
        BuyHouse buyHouse = (BuyHouse) CglibProxyFactory.create(BuyHouse.class);
        buyHouse.buy();
    }
}
