package 设计模式.代理.动态代理.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class MyInterceptor implements MethodInterceptor {
    @Override
    public Object intercept(Object proxy,
                            Method method,
                            Object[] args,
                            MethodProxy methodProxy) throws Throwable {
        System.out.println("买房准备工作...");
        Object feedback = methodProxy.invokeSuper(proxy,args);
        System.out.println("买房收尾工作...");
        return feedback;
    }
}
