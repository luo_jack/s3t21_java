package 设计模式.代理.动态代理.cglib;

import net.sf.cglib.proxy.Enhancer;

public class CglibProxyFactory {

    public static Object create(Class target){
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new MyInterceptor());
        enhancer.setSuperclass(target);
        return enhancer.create();
    }
}
