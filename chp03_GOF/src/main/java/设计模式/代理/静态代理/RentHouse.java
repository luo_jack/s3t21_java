package 设计模式.代理.静态代理;

public class RentHouse implements IRentHouse{

    public void abc(){
        System.out.println("实现租房");
    }

    @Override
    public void rentHouse() {
        abc();
    }
}
