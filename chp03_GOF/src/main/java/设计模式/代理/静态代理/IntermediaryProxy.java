package 设计模式.代理.静态代理;

//中介(代理)
public class IntermediaryProxy implements IRentHouse{

    //被代理的对象
    private IRentHouse rentHouse;

    public IntermediaryProxy(IRentHouse rentHouse) {
        this.rentHouse = rentHouse;
    }

    private void one(){
        System.out.println("交中介费");
    }

    private void two(){
        System.out.println("中介负责维修管理");
    }

    @Override
    public void rentHouse() {
        this.one();
        rentHouse.rentHouse();
        this.two();
    }
}
