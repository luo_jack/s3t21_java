package 设计模式.代理.静态代理;

public class TestStaticProxy {

    public static void main(String[] args) {
        IRentHouse rentHouse = new RentHouse();
        IRentHouse proxy = new IntermediaryProxy(rentHouse);
        IntermediaryProxy2 proxy2 = new IntermediaryProxy2(rentHouse);
        proxy.rentHouse();
        System.out.println("------------");
        proxy2.rentHouse();
    }
}
