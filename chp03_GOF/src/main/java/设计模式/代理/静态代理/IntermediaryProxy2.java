package 设计模式.代理.静态代理;

//代理2
public class IntermediaryProxy2 implements IRentHouse{
    //被代理的对象
    private IRentHouse rentHouse;

    public IntermediaryProxy2(IRentHouse rentHouse) {
        this.rentHouse = rentHouse;
    }
    @Override
    public void rentHouse() {
        System.out.println("请客户喝茶");
        rentHouse.rentHouse();
        System.out.println("打扫房间卫生");
    }
}
