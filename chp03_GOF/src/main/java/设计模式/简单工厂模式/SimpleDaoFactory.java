package 设计模式.简单工厂模式;

public class SimpleDaoFactory {

    public static NewsDao getInstance(String name){
        if(name.equals("mysql")){
            return new NewsDaoMysqlImpl();
        }else if(name.equals("xml")){
            return new NewsDaoXMLImpl();
        }else if(name.equals("oracle")){
            return new NewsDaoOracleImpl();
        }
        throw new RuntimeException("找不到实现");
    }
}
