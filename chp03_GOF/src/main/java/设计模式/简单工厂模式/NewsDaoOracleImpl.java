package 设计模式.简单工厂模式;

public class NewsDaoOracleImpl implements NewsDao{
    @Override
    public void getAll() {
        System.out.println("Oracle -- getAll()");
    }
}
