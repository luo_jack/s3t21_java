package 开闭原则.未实现;

public class Computer {

    public void runDevice(int type){
        if(type == 1){
            runMouse();
        }else if(type == 2){
            runKeyboard();
        }else if(type == 3){
            runHeadSet();
        }
    }

    public void runMouse(){
        System.out.println("运行鼠标");
    }

    public void runKeyboard(){
        System.out.println("运行键盘");
    }

    public void runHeadSet(){
        System.out.println("运行耳机");
    }


}
