package 接口隔离原则;

//运动员接口
public interface Sporter {

    void run();

    void swim();

    void bike();

    void jump();
}
