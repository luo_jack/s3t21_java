package com.example.controller;

import com.example.entity.Result;
import com.example.entity.Student;
import com.example.service.StudentService;
import org.mytools.test.MyToolsAdd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    MyToolsAdd myToolsAdd;

    @PostMapping("/login")
    public String login(Student student,HttpSession session){
        Student studentdb = studentService.login(student.getStudentNo(),student.getLoginPwd());
        if(studentdb != null){
            //登录成功
            session.setAttribute("student",studentdb);
            return "redirect:main";
        }
        return "redirect:/index";
    }

    @GetMapping("/main")
    public String main(Model model,HttpSession session){
        int count = myToolsAdd.add(3,5);
        model.addAttribute("now",new Date());
        Student student = (Student) session.getAttribute("student");
        List<Result> results = studentService.getResult(student.getStudentNo());
        model.addAttribute("results",results);
        model.addAttribute("count",count);
        return "main";
    }
}
