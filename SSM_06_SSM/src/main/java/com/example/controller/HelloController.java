package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
public class HelloController {

    @ResponseBody
    @GetMapping("/hello")
    public Object hello(){
        HashMap<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("message","成功了");
        return map;
    }

    @RequestMapping("/index")
    public String index(){
        return "index";
    }
}
