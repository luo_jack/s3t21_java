package com.example.service;


import com.example.dao.SubjectDao;
import com.example.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SubjectService {

    @Autowired
    private SubjectDao subjectDao;

    public List<Subject> getAll(){
        return subjectDao.getAll();
    }

    public Subject getOne(int subjectNo){
        return subjectDao.getOne(subjectNo);
    }

    public int update(Subject subject){
        return subjectDao.update(subject);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public int add(Subject subject){
        int count = subjectDao.add(subject);
        int a = 5/0;
        subjectDao.add(subject);
        return count;
    }

    public void addOne(){
        subjectDao.add(new Subject("one",100,1));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addTwo(){
        subjectDao.add(new Subject("twoA",201,2));
        int a = 5/0;
        subjectDao.add(new Subject("twoB",202,2));
    }
}
