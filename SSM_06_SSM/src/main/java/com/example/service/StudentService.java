package com.example.service;

import com.example.dao.ResultDao;
import com.example.dao.StudentDao;
import com.example.dao.SubjectDao;
import com.example.entity.Result;
import com.example.entity.Student;
import com.example.entity.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentDao studentDao;

    @Autowired
    ResultDao resultDao;

    @Autowired
    SubjectDao subjectDao;

    public Student login(Integer studentNo,String loginPwd){
        Student student = studentDao.getOne(studentNo, loginPwd);

        return student;
    }

    @Transactional
    public List<Result> getResult(Integer studentNo){
        List<Result> results = resultDao.getByStudentNo(studentNo);
        for(Result result:results){
            Subject subject = subjectDao.getOne(result.getSubjectNo());
            if(subject != null) {
                result.setSubjectName(subject.getSubjectName());
            }
        }
        return results;
    }
}
