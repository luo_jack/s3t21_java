package com.example.entity;

public class Subject {
    private int subjectNo;
    private String subjectName;
    private int classHour;
    private int gradeID;
    private String gradeName;

    public Subject() {
    }

    public Subject(String subjectName, int classHour, int gradeID) {
        this.subjectName = subjectName;
        this.classHour = classHour;
        this.gradeID = gradeID;
    }

    public Subject(int subjectNo, String subjectName, int classHour, int gradeID) {
        this.subjectNo = subjectNo;
        this.subjectName = subjectName;
        this.classHour = classHour;
        this.gradeID = gradeID;
    }

    public int getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(int subjectNo) {
        this.subjectNo = subjectNo;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getClassHour() {
        return classHour;
    }

    public void setClassHour(int classHour) {
        this.classHour = classHour;
    }

    public int getGradeID() {
        return gradeID;
    }

    public void setGradeID(int gradeID) {
        this.gradeID = gradeID;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "subjectNo=" + subjectNo +
                ", subjectName='" + subjectName + '\'' +
                ", classHour=" + classHour +
                ", gradeID=" + gradeID +
                ", gradeName='" + gradeName + '\'' +
                '}';
    }
}
