package com.example.entity;

import java.util.Date;

public class Result {

    private Long id;

    private Integer studentNo;

    private Integer subjectNo;

    private Date examDate;

    private Integer studentResult;

    private String subjectName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(Integer studentNo) {
        this.studentNo = studentNo;
    }

    public Integer getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(Integer subjectNo) {
        this.subjectNo = subjectNo;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public Integer getStudentResult() {
        return studentResult;
    }

    public void setStudentResult(Integer studentResult) {
        this.studentResult = studentResult;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", studentNo=" + studentNo +
                ", subjectNo=" + subjectNo +
                ", examDate=" + examDate +
                ", studentResult=" + studentResult +
                ", subjectName='" + subjectName + '\'' +
                '}';
    }
}
