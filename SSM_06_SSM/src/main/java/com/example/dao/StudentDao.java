package com.example.dao;

import com.example.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StudentDao {

    Student getOne(@Param("studentNo") Integer studentNo,
                   @Param("loginPwd") String loginPwd);
}
