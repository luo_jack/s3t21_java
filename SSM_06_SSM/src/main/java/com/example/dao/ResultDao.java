package com.example.dao;

import com.example.entity.Result;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ResultDao {

    List<Result> getByStudentNo(Integer studentNo);
}
