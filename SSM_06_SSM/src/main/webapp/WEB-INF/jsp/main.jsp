<%--
  Created by IntelliJ IDEA.
  User: 17400
  Date: 2023/5/22
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>主页</title>
</head>
<body>
    <h1>主页</h1>
    <h1>count:${count}</h1>
    <h1>${now}</h1>
    <h1>${student}</h1>

    <ul>
        <c:forEach items="${results}" var="item">
            <li>
                ${item}
            </li>
        </c:forEach>

    </ul>
</body>
</html>
