package myschool;
import java.sql.*;
public class SubjectDaoImpl implements SubjectDao{

    BaseDao baseDao = new BaseDao();

    @Override
    public Subject getOne(int subjectNo) {
        System.out.println("select * from subject where subjectNo = ?");
        Connection connection =null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try{
            connection = this.baseDao.getConnection();
            pst = connection.prepareStatement("select * from subject where subjectNo = ?");
            pst.setObject(1,subjectNo);
            rs = pst.executeQuery();
            if(rs.next()){
                Subject subject = new Subject();
                subject.setSubjectNo(rs.getInt("subjectNo"));
                subject.setSubjectName(rs.getString("subjectName"));
                subject.setClassHour(rs.getInt("classHour"));
                subject.setGradeID(rs.getInt("gradeID"));
                return subject;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
