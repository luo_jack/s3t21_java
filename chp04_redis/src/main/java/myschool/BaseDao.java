package myschool;

import java.sql.*;

public class BaseDao {

    String driverName = "com.mysql.jdbc.Driver";
    String connStr = "jdbc:mysql://localhost:3306/myschool?useSSL=false";
    String username = "root";
    String pwd = "root";

    //获取连接
    public Connection getConnection(){
        try {
            Class.forName(driverName);
            return DriverManager.getConnection(connStr, username, pwd);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    //释放资源
    public void closeAll(Connection conn, Statement st, ResultSet rs){
        try {
            if(rs != null)
                rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if(st != null)
                st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if(conn != null)
                conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BaseDao dao = new BaseDao();
        System.out.println(dao.getConnection());
    }
}
