package myschool;

public interface SubjectDao {

    Subject getOne(int subjectNo);
}
