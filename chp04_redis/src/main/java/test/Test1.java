package test;

import redis.clients.jedis.Jedis;

public class Test1 {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("localhost",6379);
//        jedis.set("hello","world");

        jedis.setex("hello",60,"world");

        boolean flag = jedis.exists("hello");
        if(flag){
            String value = jedis.get("hello");
            Long ttl = jedis.ttl("hello");
            System.out.println(value+" ttl:"+ttl);
        }else{
            System.out.println("该键不存在");
        }

    }
}
