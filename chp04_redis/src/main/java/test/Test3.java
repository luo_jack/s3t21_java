package test;

import redis.clients.jedis.Jedis;
import util.SerializeUtils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Test3 {

    public static void main(String[] args) {
        //存对象 序列化
        Student student = new Student();
        student.setId(1003);
        student.setName("马克");
        student.setAge(17);

        Jedis jedis = new Jedis("localhost",6379);
        byte[] ar1 = "stu1003".getBytes(StandardCharsets.UTF_8);
        System.out.println(Arrays.toString(ar1));
        byte[] ar2 = SerializeUtils.serialize(student);
        System.out.println(Arrays.toString(ar2));

        jedis.set(ar1,ar2);
        byte[] ar3 = jedis.get(ar1);
        Student student2 = (Student) SerializeUtils.deSerialize(ar3);
        System.out.println(student2);
        //0 - 255
        //-128 - 127
//        jedis.set()
    }
}
