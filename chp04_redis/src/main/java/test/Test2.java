package test;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;

import java.nio.charset.StandardCharsets;

public class Test2 {

    public static void main(String[] args) {
        //存对象
        Student student = new Student();
        student.setId(1001);
        student.setName("杰克");
        student.setAge(29);

        Jedis jedis = new Jedis("localhost",6379);

        jedis.set("stu1001",student.toString());
        String abc = jedis.get("stu1001");
        System.out.println(abc);
//         Student student1 = jedis.get("stu1001");
        Gson gson = new Gson();
        Student student1 = gson.fromJson(abc,Student.class);
        System.out.println("姓名:"+student1.getName());
        System.out.println("年龄:"+student1.getAge());
    }
}
