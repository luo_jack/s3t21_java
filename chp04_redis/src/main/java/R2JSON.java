import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import test.Student;

public class R2JSON {

    public static void main(String[] args) {
        Student S1 = new Student();
        S1.setId(1001);
        S1.setName("Jack");
        S1.setAge(18);
        Gson gson = new Gson();
        String JS1 = gson.toJson(S1);
        Jedis jedis = new Jedis("localhost",6379);
        jedis.set("JS1",JS1);
        String JS2 = jedis.get("JS1");
        System.out.println(JS2);
        Student S2 = null;
        S2 = gson.fromJson(JS2,Student.class);
        System.out.println(S2.getId());
        System.out.println(S2.getName());
        System.out.println(S2.getAge());
    }

}
