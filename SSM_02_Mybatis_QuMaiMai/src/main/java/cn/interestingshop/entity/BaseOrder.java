package cn.interestingshop.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseOrder {
//`id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    private int id;
//`userId` int(255) DEFAULT NULL COMMENT '用户主键',
    private int userId;
//`account` varchar(255) DEFAULT NULL,
    private String account;
//`userAddress` varchar(255) DEFAULT NULL COMMENT '用户地址',
    private String userAddress;
//`createTime` datetime DEFAULT NULL COMMENT '创建时间',
    private Date createTime;
//`amount` float DEFAULT NULL COMMENT '总消费',
    private double amount;
//`orderNo` varchar(255) DEFAULT NULL COMMENT '订单号',
    private String orderNo;

    private List<OrderInfo> orderInfos = new ArrayList<>();

    public List<OrderInfo> getOrderInfos() {
        return orderInfos;
    }

    public void setOrderInfos(List<OrderInfo> orderInfos) {
        this.orderInfos = orderInfos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return "BaseOrder{" +
                "id=" + id +
                ", userId=" + userId +
                ", account='" + account + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", createTime=" + createTime +
                ", amount=" + amount +
                ", orderNo='" + orderNo + '\'' +
                ", orderInfos=" + orderInfos +
                '}';
    }
}
