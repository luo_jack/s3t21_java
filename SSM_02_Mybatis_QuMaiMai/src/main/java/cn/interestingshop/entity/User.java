package cn.interestingshop.entity;

public class User {

//Field   Type    Comment
//id  int(20) NOT NULL主键
    private int id;
//account varchar(255) NOT NULL账号
    private String account;
//nickName    varchar(255) NOT NULL昵称
    private String nickName;
//password    varchar(255) NOT NULL密码
    private String password;
//gender  int(1) NOT NULL性别(1:男 0：女)
    private int gender;
//idCardNo    varchar(60) NULL身份证号
    private String idCardNo;
//email   varchar(80) NULL邮箱
    private String email;
//phone   varchar(11) NULL手机
    private String phone;
//type    int(2) NULL类型（1：后台 0:前台）
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", nickName='" + nickName + '\'' +
                ", password='" + password + '\'' +
                ", gender=" + gender +
                ", idCardNo='" + idCardNo + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                '}';
    }
}
