package cn.interestingshop.entity;

public class Goods {
//    `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
    private int id;
//    `goodsName` varchar(20) NOT NULL COMMENT '名称',
    private String goodsName;
//    `goodsDesc` varchar(1024) DEFAULT NULL COMMENT '描述',
    private String goodsDesc;
//    `price` float NOT NULL COMMENT '价格',
    private double price;
//    `stock` int(10) NOT NULL COMMENT '库存',
    private int stock;
//    `classifyLevel1Id` int(10) DEFAULT NULL COMMENT '分类1',
    private Classify classifyLevel1;
//    `classifyLevel2Id` int(10) DEFAULT NULL COMMENT '分类2',
    private Classify classifyLevel2;
//    `classifyLevel3Id` int(10) DEFAULT NULL COMMENT '分类3',
    private Classify classifyLevel3;
//    `fileName` varchar(200) DEFAULT NULL COMMENT '文件名称',
    private String fileName;
//    `isDelete` int(1) DEFAULT '0' COMMENT '是否删除(1：删除 0：未删除)'
    private boolean isDelete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Classify getClassifyLevel1() {
        return classifyLevel1;
    }

    public void setClassifyLevel1(Classify classifyLevel1) {
        this.classifyLevel1 = classifyLevel1;
    }

    public Classify getClassifyLevel2() {
        return classifyLevel2;
    }

    public void setClassifyLevel2(Classify classifyLevel2) {
        this.classifyLevel2 = classifyLevel2;
    }

    public Classify getClassifyLevel3() {
        return classifyLevel3;
    }

    public void setClassifyLevel3(Classify classifyLevel3) {
        this.classifyLevel3 = classifyLevel3;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", goodsName='" + goodsName + '\'' +
                ", goodsDesc='" + goodsDesc + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", classifyLevel1=" + classifyLevel1 +
                ", classifyLevel2=" + classifyLevel2 +
                ", classifyLevel3=" + classifyLevel3 +
                ", fileName='" + fileName + '\'' +
                ", isDelete=" + isDelete +
                '}';
    }
}
