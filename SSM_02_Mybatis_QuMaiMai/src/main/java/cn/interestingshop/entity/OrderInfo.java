package cn.interestingshop.entity;

//订单明细
public class OrderInfo {
//`id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
    private int id;
//`baseOrderId` int(10) NOT NULL COMMENT '订单主键',
    private int baseOrderId;
//`goodsId` int(10) NOT NULL COMMENT '商品主键',
    private int goodsId;
//`buyNum` int(10) NOT NULL COMMENT '数量',
    private int buyNum;
//`amount` float NOT NULL COMMENT '消费'
    private double amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBaseOrderId() {
        return baseOrderId;
    }

    public void setBaseOrderId(int baseOrderId) {
        this.baseOrderId = baseOrderId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(int buyNum) {
        this.buyNum = buyNum;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "id=" + id +
                ", baseOrderId=" + baseOrderId +
                ", goodsId=" + goodsId +
                ", buyNum=" + buyNum +
                ", amount=" + amount +
                '}';
    }
}
