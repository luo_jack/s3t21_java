package cn.interestingshop.test.step03;

import cn.interestingshop.dao.BaseOrderDao;
import cn.interestingshop.dao.OrderInfoDao;
import cn.interestingshop.entity.BaseOrder;
import cn.interestingshop.entity.OrderInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Test_Buy {

    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
        OrderInfoDao orderInfoDao = session.getMapper(OrderInfoDao.class);
        //733 1
        //740 2
        Map<Integer,Integer> items = new HashMap<>();
        items.put(733,1);
        items.put(740,2);

        BaseOrder baseOrder = new BaseOrder();
        baseOrder.setAccount("admin");
        baseOrder.setAmount(1000);
        baseOrder.setCreateTime(new Date());
        baseOrder.setUserAddress("北京路");
        baseOrder.setUserId(2);
        String uuid = UUID.randomUUID().toString().toUpperCase().replaceAll("-","");
        baseOrder.setOrderNo(uuid);
        baseOrderDao.add(baseOrder);
        System.out.println(baseOrder.getId());

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setBaseOrderId(999);
        orderInfo.setGoodsId(1);
        orderInfo.setBuyNum(10);
        orderInfo.setAmount(100);

        //orderInfoDao.add(orderInfo);
        session.commit();
    }
}
