package cn.interestingshop.test.step03;

import cn.interestingshop.dao.BaseOrderDao;
import cn.interestingshop.entity.BaseOrder;
import cn.interestingshop.entity.OrderInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test03_05 {

    public static void main(String[] args) throws IOException {
        //查询订单 id
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
        BaseOrder baseOrder = baseOrderDao.getOne(23);
        System.out.println(baseOrder.getOrderNo()+"\t"+baseOrder.getAmount());
        for (OrderInfo orderInfo:baseOrder.getOrderInfos()){
            System.out.println(orderInfo);
        }
    }
}
