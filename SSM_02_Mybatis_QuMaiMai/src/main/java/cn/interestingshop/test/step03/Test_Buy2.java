package cn.interestingshop.test.step03;

import cn.interestingshop.dao.BaseOrderDao;
import cn.interestingshop.dao.GoodsDao;
import cn.interestingshop.dao.OrderInfoDao;
import cn.interestingshop.entity.BaseOrder;
import cn.interestingshop.entity.Goods;
import cn.interestingshop.entity.OrderInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Test_Buy2 {

    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        BaseOrderDao baseOrderDao = session.getMapper(BaseOrderDao.class);
        OrderInfoDao orderInfoDao = session.getMapper(OrderInfoDao.class);
        GoodsDao goodsDao = session.getMapper(GoodsDao.class);
        //733 1
        //740 2
        //购物清单
        Map<Integer,Integer> items = new HashMap<>();
        items.put(733,1);
        items.put(740,2);

        try{
            List<OrderInfo> orderInfoList = new ArrayList<>();
            BaseOrder baseOrder = new BaseOrder();
            for(Integer gid: items.keySet()){
                Goods goods = goodsDao.getOne(gid);
                //生成订单明细对象
                OrderInfo orderInfo = new OrderInfo();
                orderInfo.setGoodsId(gid);
                orderInfo.setBuyNum(items.get(gid));
                //计算单个订单明细价
                orderInfo.setAmount(goods.getPrice() * orderInfo.getBuyNum());
                orderInfoList.add(orderInfo);
                //累加总价
                baseOrder.setAmount(baseOrder.getAmount()+ orderInfo.getAmount());
            }
            //设置订单其余信息
            baseOrder.setAccount("admin");
            baseOrder.setCreateTime(new Date());
            baseOrder.setUserAddress("北京路");
            baseOrder.setUserId(2);
            String uuid = UUID.randomUUID().toString().toUpperCase().replaceAll("-","");
            baseOrder.setOrderNo(uuid);
            //插入订单总表
            baseOrderDao.add(baseOrder);
            //循环设置订单明细所属于哪个订单
            for(OrderInfo orderInfo: orderInfoList){
                orderInfo.setBaseOrderId(baseOrder.getId());
                //插入订单明细表
                orderInfoDao.add(orderInfo);
                //修改商品库存
                Goods goods = goodsDao.getOne(orderInfo.getGoodsId());
                if(goods.getStock()< orderInfo.getBuyNum()){
                    throw new RuntimeException("库存不足");
                }
                goodsDao.updateStock(goods.getId(), goods.getStock()- orderInfo.getBuyNum());
            }
            session.commit();
        }catch (Exception ex){
            session.rollback();
            System.out.println("回滚业务");
        }

    }
}
