package cn.interestingshop.test.step02;

import cn.interestingshop.dao.GoodsDao;
import cn.interestingshop.entity.Classify;
import cn.interestingshop.entity.Goods;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test02_01 {

    public static void main(String[] args) throws IOException {
        //添加商品
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        GoodsDao goodsDao = session.getMapper(GoodsDao.class);
        Goods goods = new Goods();
        goods.setGoodsName("测试商品");
        goods.setGoodsDesc("测试商品描述");
        goods.setPrice(3.5);
        goods.setStock(100);
        Classify c1 = new Classify();
        c1.setId(548);
        Classify c2 = new Classify();
        c2.setId(654);
        Classify c3 = new Classify();
        c3.setId(655);
        goods.setClassifyLevel1(c1);
        goods.setClassifyLevel2(c2);
        goods.setClassifyLevel3(c3);
        goods.setFileName(null);
        goods.setDelete(false);
        System.out.println(goodsDao.add(goods));
    }
}
