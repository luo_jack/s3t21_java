package cn.interestingshop.test.step02;

import cn.interestingshop.dao.GoodsDao;
import cn.interestingshop.entity.Goods;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test02_02 {

    public static void main(String[] args) throws IOException {
        //修改商品信息
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        GoodsDao goodsDao = session.getMapper(GoodsDao.class);
        Goods goods = goodsDao.getOne(777);
        goods.setStock(goods.getStock()-1);
        System.out.println(goodsDao.update(goods));
    }
}
