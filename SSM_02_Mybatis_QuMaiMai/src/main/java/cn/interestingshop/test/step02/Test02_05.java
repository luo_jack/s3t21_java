package cn.interestingshop.test.step02;

import cn.interestingshop.dao.GoodsDao;
import cn.interestingshop.entity.Goods;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class Test02_05 {

    public static void main(String[] args) throws IOException {
        //商品分页查询
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        GoodsDao goodsDao = session.getMapper(GoodsDao.class);
        HashMap<String,Object> params = new HashMap<>();
        params.put("goodsName","华");
        params.put("cid1",670);
        List<Goods> goodsList = goodsDao.getList(params,1,3);
        //获取总行数
        int count = goodsDao.getCount(params);
        for (Goods goods:goodsList){
            System.out.println(goods);
        }
        System.out.println("总行数:"+count);
    }
}
