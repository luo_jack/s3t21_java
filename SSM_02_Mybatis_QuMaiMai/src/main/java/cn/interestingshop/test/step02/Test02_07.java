package cn.interestingshop.test.step02;

import cn.interestingshop.dao.GoodsDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test02_07 {

    public static void main(String[] args) throws IOException {
        //更新库存
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        GoodsDao goodsDao = session.getMapper(GoodsDao.class);
        System.out.println(goodsDao.updateStock(777,200));
    }
}
