package cn.interestingshop.test.step01;

import cn.interestingshop.dao.UserDao;
import cn.interestingshop.entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test01_01 {

    public static void main(String[] args) throws IOException {
        //增加用户
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        UserDao userDao = session.getMapper(UserDao.class);
        User user = new User();
        user.setAccount("jack");
        user.setNickName("杰克");
        user.setPassword("jack");
        user.setGender(1);
        user.setIdCardNo("76512314");
        user.setEmail("jack@qq.com");
        user.setPhone("789");
        user.setType(1);

        System.out.println(userDao.add(user));
        session.commit();
    }
}
