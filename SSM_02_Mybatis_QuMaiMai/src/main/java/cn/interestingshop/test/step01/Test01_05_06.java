package cn.interestingshop.test.step01;

import cn.interestingshop.dao.UserDao;
import cn.interestingshop.entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test01_05_06 {
    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        UserDao userDao = session.getMapper(UserDao.class);
        int pageIndex = 1;//第几页
        int pageRows = 3;//每页行数
        int totalPage = 0;//总页数
        List<User> userList = userDao.getByPage(pageIndex,pageRows);
        int totalRows = userDao.getCount();//总行数
        for(User user : userList){
            System.out.println(user);
        }

        System.out.println("总行数:"+totalRows);
        if(totalRows % pageRows == 0){
            totalPage = totalRows / pageRows;
        }else{
            totalPage = totalRows / pageRows + 1;
        }
        System.out.println("当前页数:"+pageIndex);
        System.out.println("总页数:"+totalPage);
    }
}
