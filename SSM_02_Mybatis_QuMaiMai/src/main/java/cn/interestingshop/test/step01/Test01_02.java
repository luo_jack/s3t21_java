package cn.interestingshop.test.step01;

import cn.interestingshop.dao.UserDao;
import cn.interestingshop.entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test01_02 {
    public static void main(String[] args) throws IOException {
        //修改用户信息
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession(true);
        UserDao userDao = session.getMapper(UserDao.class);
        User user = userDao.getOne(25,null);
        user.setPhone("6789");
        System.out.println(userDao.update(user));

    }
}
