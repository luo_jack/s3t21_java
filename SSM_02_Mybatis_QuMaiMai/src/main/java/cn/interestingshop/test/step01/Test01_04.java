package cn.interestingshop.test.step01;

import cn.interestingshop.dao.UserDao;
import cn.interestingshop.entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test01_04 {

    public static void main(String[] args) throws IOException {
        //根据用户id或登录名查询用户信息
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        UserDao userDao = session.getMapper(UserDao.class);
        System.out.println(userDao.getOne(25,null));
        System.out.println(userDao.getOne(null,"sb"));
    }
}
