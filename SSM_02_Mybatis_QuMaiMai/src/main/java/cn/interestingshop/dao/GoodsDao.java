package cn.interestingshop.dao;

import cn.interestingshop.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface GoodsDao {

    int add(Goods goods);

    int update(Goods goods);

    int updateStock(@Param("id") int id,@Param("stock") int stock);

    Goods getOne(int id);

    List<Goods> getList(@Param("queryParams") HashMap<String,Object> queryParams,
                        @Param("pageIndex") int pageIndex,
                        @Param("pageRows") int pageRows);

    int getCount(@Param("queryParams") HashMap<String,Object> queryParams);
}
