package cn.interestingshop.dao;

import cn.interestingshop.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    int add(User user);

    int update(User user);

    int delete(int id);

    User getOne(@Param("id") Integer id,@Param("account") String account);

    List<User> getByPage(@Param("pageIndex") int pageIndex,@Param("pageRows") int pageRows);

    int getCount();
}
