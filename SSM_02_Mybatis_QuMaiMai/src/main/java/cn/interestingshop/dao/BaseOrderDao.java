package cn.interestingshop.dao;

import cn.interestingshop.entity.BaseOrder;

public interface BaseOrderDao {

    BaseOrder getOne(int id);

    int add(BaseOrder baseOrder);
}
