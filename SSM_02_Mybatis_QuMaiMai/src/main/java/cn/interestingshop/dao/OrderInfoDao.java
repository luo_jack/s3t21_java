package cn.interestingshop.dao;

import cn.interestingshop.entity.OrderInfo;

public interface OrderInfoDao {

    int add(OrderInfo orderInfo);
}
