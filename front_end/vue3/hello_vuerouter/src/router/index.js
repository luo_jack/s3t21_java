import { createRouter, createWebHistory } from 'vue-router'
import HomeView from "../views/HomeView.vue";
import SettingView from "../views/SettingView.vue";
import AboutView from "../views/AboutView.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path:'/',
      name:'home',
      component:HomeView,
      redirect:'/emp1',
      children:[
        {
          path: 'emp1',
          component: () => import('../views/employee/EmployeeView1.vue')
        },
        {
          path: 'emp2',
          component: () => import('../views/employee/EmployeeView2.vue')
        },
        {
          path: 'emp3',
          component: () => import('../views/employee/EmployeeView3.vue')
        },
        {
          path: 'man1',
          component: () => import('../views/manager/ManagerView1.vue')
        },
        {
          path: 'man2',
          component: () => import('../views/manager/ManagerView2.vue')
        },
        {
          path: 'man3',
          component: () => import('../views/manager/ManagerView3.vue')
        },
        {
          path: 'boss1',
          component: () => import('../views/boss/BossView1.vue')
        },
        {
          path: 'boss2',
          component: () => import('../views/boss/BossView2.vue')
        },
        {
          path: 'boss3',
          component: () => import('../views/boss/BossView3.vue')
        },
      ]
    },
    {
      path:'/setting',
      name:'setting',
      component:SettingView
    },
    {
      path:'/about',
      name:'about',
      component:AboutView
    },
  ]
})

export default router
