export default {
    props:{
        msg:String
    },
    computed:{
        title(){
            return this.msg
        }
    },
    data(){
      return {
          //接收属性成为data
      }
    },
    emits:['ok','cancel'],
    methods:{
        doOK(){
            //触发了ok事件
            this.$emit('ok')
        },
        doCancel(){
            //触发了cancel事件
            this.$emit('cancel')
        }
    },
    template: `
  <h2>A Child Component!!! | {{msg}} | {{title}}</h2>
  <slot></slot>
  <button @click="doOK">ok</button>
  <button @click="doCancel">cancel</button>
  `
}