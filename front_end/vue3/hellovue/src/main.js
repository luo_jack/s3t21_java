import { createApp } from 'vue'
import { createRouter, createWebHashHistory,createWebHistory } from 'vue-router'
import App from './App.vue'
import GradeListView from "./views/GradeListView.vue";
import SubjectListView from "./views/SubjectListView.vue";
import SubjectListView2 from "./views/SubjectListView2.vue";
import RowView from "./views/RowView.vue";
import SubjectUpdateView from "./views/SubjectUpdateView.vue";
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

const routes = [
    {
      path:'/',
      redirect:'/grade'
    },
    {
        path: '/grade',
        name:'grade',
        component: GradeListView
    },
    {
        path: '/subject',
        name:'subject',
        component: SubjectListView
    },
    {
        path: '/subject2',
        name:'subject2',
        component: SubjectListView2
    },
    {
        path: '/row',
        name:'row',
        component: RowView
    },
    {
        path: '/subjectupdate/:id',
        name: 'subjectupdate',
        component: SubjectUpdateView
    }
]

const router = createRouter({
    history:createWebHistory(),
    routes
})

createApp(App).use(router).use(Antd).mount('#app')
