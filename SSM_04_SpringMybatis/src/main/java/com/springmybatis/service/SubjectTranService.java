package com.springmybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SubjectTranService {

    @Autowired
    private SubjectService subjectService;

    @Transactional
    public void addTran(){
        try{
            subjectService.addOne();
            subjectService.addTwo();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

    }
}
