package com.springmybatis.dao;


import com.springmybatis.entity.Subject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SubjectDao {
    List<Subject> getAll();

    List<Subject> getByName(String subjectName);

    List<Subject> getByNameAndGradeID(@Param("subjectName") String subjectName,
                                      @Param("gradeID") int gradeID);

    List<Subject> getByQuery(Subject subject);

    //多条件查询
    List<Subject> getByQuery2(Map<String,Object> subjectMap);

    //不定条件查询
    List<Subject> getByQuery3(Map<String,Object> subjectMap);

    //不定条件查询(choose)
    List<Subject> getByQuery4(Map<String,Object> subjectMap);

    //trim实现where功能
    List<Subject> getByQuery5(Map<String,Object> subjectMap);

    Subject getOne(int subjectNo);

    //使用resultmap
    Subject getOne2(int subjectNo);

    int add(Subject subject);

    //全量更新
    int update(Subject subject);

    //有条件更新
    int update2(Subject subject);

    int delete(int subjectNo);
}
