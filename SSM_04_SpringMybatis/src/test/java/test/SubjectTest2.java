package test;

import com.springmybatis.entity.Subject;
import com.springmybatis.service.SubjectService;
import com.springmybatis.service.SubjectTranService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SubjectTest2 {

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private SubjectTranService subjectTranService;

    @Test
    public void test1(){
        System.out.println(subjectService.getAll());
    }

    @Test
    public void test2(){
        subjectService.add(new Subject("test2",100,1));
    }

    @Test
    public void test3(){
        subjectTranService.addTran();
    }
}
