package com.mybatis.entity;

import java.util.Date;

public class Result {

    private long id;

    private Student student;

    private Subject subject;

    private Date examDate;

    private int studentResult;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public int getStudentResult() {
        return studentResult;
    }

    public void setStudentResult(int studentResult) {
        this.studentResult = studentResult;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", student=" + student +
                ", subject=" + subject +
                ", examDate=" + examDate +
                ", studentResult=" + studentResult +
                '}';
    }
}
