package com.mybatis.entity;

import java.util.ArrayList;
import java.util.List;

public class Grade {

    private int gradeID;

    private String gradeName;

    private List<Subject> subjectList = new ArrayList<>();

    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    public int getGradeID() {
        return gradeID;
    }

    public void setGradeID(int gradeID) {
        this.gradeID = gradeID;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "gradeID=" + gradeID +
                ", gradeName='" + gradeName + '\'' +
                ", subjectList=" + subjectList +
                '}';
    }
}
