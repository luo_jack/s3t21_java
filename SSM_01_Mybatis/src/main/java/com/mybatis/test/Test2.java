package com.mybatis.test;

import com.mybatis.dao.GradeDao;
import com.mybatis.entity.Grade;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test2 {

    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        GradeDao gradeDao = session.getMapper(GradeDao.class);
//        System.out.println(gradeDao);
//        List<Grade> grades = gradeDao.getAll();
//        for(Grade grade:grades){
//            System.out.println(grade);
//        }

//        Grade grade = gradeDao.getOne(32);
//        grade.setGradeName("明年");
//        gradeDao.update(grade);
//        session.commit();

//        gradeDao.delete(55);
//        session.commit();

        System.out.println(gradeDao.getOneBySubject(1));
    }
}
