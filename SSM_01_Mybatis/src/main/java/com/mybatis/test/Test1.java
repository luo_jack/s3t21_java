package com.mybatis.test;

import com.mybatis.entity.Grade;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test1 {

    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
//        List<Grade> grades = session.selectList("com.mybatis.dao.GradeDao.getAll");
//        for(Grade grade:grades){
//            System.out.println(grade);
//        }

        Grade grade = new Grade();
        grade.setGradeName("什么牛奶");
        int flag = session.insert("com.mybatis.dao.GradeDao.add",grade);
        session.commit();
        System.out.println(flag);
    }
}
