package com.mybatis.test;

import com.mybatis.dao.ResultDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class ResultTest {
    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        ResultDao resultDao = session.getMapper(ResultDao.class);
//        System.out.println(resultDao.getOne(1));
        System.out.println(resultDao.getOne2(1));
//        System.out.println(resultDao.getOne3(1));
    }
}
