package com.mybatis.test;

import com.mybatis.dao.SubjectDao;
import com.mybatis.entity.Subject;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubjectTest {
    public static void main(String[] args) {
        //创建一个输入流获取xml
        try (InputStream is = Resources.getResourceAsStream("mybatis-config.xml");) {
            // 读取输入流
            SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
            // 获取自动提交的会话,默认为false
            SqlSession session = factory.openSession(true);
            //生成接口的实现类
            SubjectDao subjectDao = session.getMapper(SubjectDao.class);
            //查询操作
//            List<Subject> list = subjectDao.getAll();
//            System.out.println(list);
//            Subject subject = subjectDao.getOne(1);
//            System.out.println(subject);
//            System.out.println(subjectDao.getOne2(1));

            //增加操作
//            Subject subject1 = new Subject("html5", 100, 2);
//            int addRes = subjectDao.add(subject1);
//            //修改操作
//            Subject subject2 = new Subject(45, "vue2", 110, 3);
//            int updateRes = subjectDao.update(subject2);
//            //删除操作
//            int deleteRes = subjectDao.delete(46);


//            System.out.println(subjectDao.getByName("a"));
//            System.out.println(subjectDao.getByNameAndGradeID("a",1));

//            Subject subject = new Subject();
//            subject.setSubjectName("a");
//            subject.setGradeID(1);
//            System.out.println(subjectDao.getByQuery(subject));

//            Map<String,Object> map = new HashMap<>();
//            map.put("subjectName","a");
//            map.put("gradeID",1);
//            map.put("min",150);
//            map.put("max",999999);
//            System.out.println(subjectDao.getByQuery2(map));

            Map<String,Object> map = new HashMap<>();
            map.put("subjectName","a");
//            map.put("gradeID",1);
//            map.put("min",100);
//            map.put("max",999999);
//            System.out.println(subjectDao.getByQuery3(map));
//            System.out.println(subjectDao.getByQuery4(map));
            System.out.println(subjectDao.getByQuery5(map));

//            Subject subject = subjectDao.getOne(1);
//            System.out.println(subject);
//            Subject subject = new Subject();
//            subject.setSubjectNo(45);
//            subject.setGradeID(2);
//            subject.setClassHour(100);
//            subjectDao.update2(subject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
