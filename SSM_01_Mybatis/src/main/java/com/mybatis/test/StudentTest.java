package com.mybatis.test;

import com.mybatis.dao.StudentDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StudentTest {

    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);

        SqlSession session = factory.openSession();
        StudentDao studentDao = session.getMapper(StudentDao.class);
//        System.out.println(studentDao.getAll());
//        System.out.println(studentDao.getOne(10000));

//        List<Integer> nos = new ArrayList<>();
//        nos.add(10001);
//        nos.add(10002);
//        nos.add(20010);
//        System.out.println(studentDao.getByNos(nos));

        System.out.println(studentDao.getStudentPageList(null,"女",3,3));
    }
}
