package com.mybatis.dao;

import com.mybatis.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StudentDao {

    List<Student> getAll();

    //pageIndex:1,2,3
    List<Student> getStudentPageList(@Param("studentName") String studentName,
                                     @Param("sex") String sex,
                                     @Param("pageIndex") Integer pageIndex,
                                     @Param("pageSize") Integer pageSize);

    List<Student> getByNos(@Param("nos") List<Integer> nos);

    Student getOne(int id);

    int add(Student student);

    int update(Student student);

    int delete(int id);
}
