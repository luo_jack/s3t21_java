package com.mybatis.dao;

import com.mybatis.entity.Grade;

import java.util.List;

public interface GradeDao {

    List<Grade> getAll();

    Grade getOne(int id);

    Grade getOneBySubject(int id);

    int add(Grade grade);

    int update(Grade grade);

    int delete(int id);
}
