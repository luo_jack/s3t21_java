package com.mybatis.dao;

import com.mybatis.entity.Result;

public interface ResultDao {

    Result getOne(int id);

    //联表
    Result getOne2(int id);

    Result getOne3(int id);
}
