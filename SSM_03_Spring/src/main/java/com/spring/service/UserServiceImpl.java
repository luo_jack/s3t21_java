package com.spring.service;

import com.spring.dao.UserDao;
import com.spring.dao.UserDaoImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService{

    private static final Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Override
    public void add() {
        System.out.println("UserServiceImpl...add()");

        userDao.add();
    }

    @Override
    public void delete() {
//        System.out.println();
        log.debug("接收到参数:98");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("删除数据成功:98");
//        throw new RuntimeException("xxx");
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
