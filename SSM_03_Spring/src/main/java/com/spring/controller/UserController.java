package com.spring.controller;

import com.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserController{

    @Resource
    private UserService userService;

    public void add(){
        System.out.println("UserController...add()");
        userService.add();
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
