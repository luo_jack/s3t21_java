package com.spring.test.abc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestABC {

    public static void main(String[] args) {
        ApplicationContext context
                = new ClassPathXmlApplicationContext("applicationContext.xml");
        A a = context.getBean(A.class);//a里面的b,b里面的c,都能够被注入(依赖注入)
        System.out.println(a);
    }
}
