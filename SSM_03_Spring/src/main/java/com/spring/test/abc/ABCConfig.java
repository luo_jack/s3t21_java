package com.spring.test.abc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ABCConfig {

    @Bean
    public A a(B b){
        A a = new A();
        a.setB(b);
        return a;
    }

    @Bean
    public B b(C c){
        B b = new B();
        b.setC(c);
        return b;
    }

    @Bean
    public C c(){
        C c = new C();
        return c;
    }
}
