package com.spring.test.abc;

public class B {

    private C c;

    public void setC(C c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "B{" +
                "c=" + c +
                '}';
    }
}
