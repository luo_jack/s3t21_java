package com.spring.test;

import com.spring.controller.UserController;
import com.spring.dao.UserDao;
import com.spring.dao.UserDaoImpl;
import com.spring.service.UserService;
import com.spring.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class UserConfig {

    @Bean
    public UserController userController(UserService userService){
        UserController userController = new UserController();
        userController.setUserService(userService);
        return userController;
    }

    @Bean
    public UserService userService(UserDao userDao){
        UserServiceImpl userService = new UserServiceImpl();
        userService.setUserDao(userDao);
        return userService;
    }

    @Bean
    public UserDao userDao(){
        UserDao userDao = new UserDaoImpl();
        return userDao;
    }
}
