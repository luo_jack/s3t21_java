package com.spring.test.box;

public class Box {
    public Box() {
        System.out.println("Box 无参构造函数被调用");
    }

    public Box(int width, int height) {
        System.out.println("Box 有参构造函数被调用..."+width+","+height);
        this.width = width;
        this.height = height;
    }

    private int width;

    private int height;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        System.out.println("setWidth..."+width);
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        System.out.println("setHeight..."+height);
        this.height = height;
    }

    @Override
    public String toString() {
        return "Box{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
