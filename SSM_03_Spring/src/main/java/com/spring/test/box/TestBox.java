package com.spring.test.box;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBox {

    public static void main(String[] args) {
        //Box box = new Box();//控制不反转，需要什么东西，自己new
        ApplicationContext context
                = new ClassPathXmlApplicationContext("applicationContext.xml");
        Box box1 = (Box) context.getBean("box1");//控制反转，需要什么东西，问spring拿
        Box box2 = (Box) context.getBean("box2");
        Box box3 = (Box) context.getBean("box3");
        System.out.println(box1);
        System.out.println(box2);
        System.out.println(box3);
        //如果有两个符合条件的bean满足，则报错
        //Box box3 = context.getBean(Box.class);
    }
}
