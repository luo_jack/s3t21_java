package com.spring.test.box;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBox2 {
    public static void main(String[] args) {
        //Box box = new Box();//控制不反转，需要什么东西，自己new
        ApplicationContext context
                = new ClassPathXmlApplicationContext("applicationContext.xml");

        Box box11 = (Box) context.getBean("box11");
        System.out.println(box11);
    }
}
