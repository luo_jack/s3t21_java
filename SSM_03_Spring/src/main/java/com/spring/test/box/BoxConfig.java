package com.spring.test.box;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BoxConfig {

    @Bean
    public Box box11(){
        Box box = new Box();
        box.setWidth(100);
        box.setHeight(101);
        return box;
    }

    @Bean
    public Box box12(){
        Box box = new Box();
        box.setWidth(210);
        box.setHeight(211);
        return box;
    }
}
