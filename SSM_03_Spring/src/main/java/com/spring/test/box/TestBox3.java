package com.spring.test.box;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBox3 {

    public static void main(String[] args) {
        ApplicationContext context
                = new ClassPathXmlApplicationContext("applicationContext.xml");

        BoxContainer boxContainer = context.getBean(BoxContainer.class);
        boxContainer.show();
    }
}
