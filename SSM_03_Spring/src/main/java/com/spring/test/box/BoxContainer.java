package com.spring.test.box;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BoxContainer {

    @Resource(name = "box12")
    private Box box;

    public void setBox(Box box) {
        this.box = box;
    }
    public void show(){
        System.out.println(box);
    }
}
