package com.spring.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(1)
public class TimeAspect {

    private static final Logger log = Logger.getLogger(TimeAspect.class);

    @Around("execution(* com.spring.service.*.*(..))")
    public Object a(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        log.debug("开始时间:" + start);
        Object obj = joinPoint.proceed();
        long end = System.currentTimeMillis();
        log.debug("结束时间:" + end);
        long timeCost = end - start;
        log.debug("消耗时间:" + timeCost);
        if(timeCost>=3000){
            log.warn("消耗时间过长，发出改进通知");
        }
        return obj;
    }
}
