package com.spring.aspect;

import com.spring.service.UserServiceImpl;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

//日志切面
@Component
@Aspect
@Order(2)
public class LoggerAspect {

    private static final Logger log = Logger.getLogger(LoggerAspect.class);

//    @Before("execution(* com.spring.service.*.*(..))")
////    @After()
////    @AfterThrowing
////    @AfterReturning
////    @Around()
//    public void a(JoinPoint joinPoint){
//        System.out.println(joinPoint.getSignature().getName()+"前置方法被调用,"+joinPoint.getTarget());
//    }
//
//    @After("execution(* com.spring.service.*.*(..))")
//    public void b(){
//        System.out.println("最终增强...");
//    }
//
//    @AfterReturning(pointcut = "execution(* com.spring.service.*.*(..))",returning = "returnValue")
//    public void c(Object returnValue){
//        System.out.println("后置增强...方法返回值:"+returnValue);
//    }
//
//    @AfterThrowing(pointcut = "execution(* com.spring.service.*.*(..))",throwing = "ex")
//    public void d(RuntimeException ex){
//        System.out.println("抛出异常增强...异常内容:"+ex.getMessage());
//    }

    @Around("execution(* com.spring.service.*.*(..))")
    public Object e(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug("环绕--开始");
        try {
            //真正的调用方法
            Object result = joinPoint.proceed();
            log.debug("环绕--方法返回值:"+result);
            return result;
        } catch (Throwable throwable) {
            log.warn("环绕--catch"+throwable.getMessage());
            throw throwable;
        }finally {
            log.debug("环绕--结束");
        }

    }
}
